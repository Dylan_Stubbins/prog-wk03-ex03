﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk03_ex03
{
    class Program
    {
        static void Main(string[] args)
        {
            var colors = new string[5] { "red", "blue", "orange", "white", "black" };
            var join = string.Join(",", colors);
            Console.WriteLine(join);
        }
    }
}
